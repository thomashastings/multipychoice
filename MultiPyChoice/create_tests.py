# These functions enable the creation of individual tests and a solution database
#     a custom number of question databases

import csv
import os
import random
from openpyxl import load_workbook
from openpyxl import Workbook
import time
import xml.etree.cElementTree as ET

def get_questions_texts(testbank, question_numbers):
    questions_texts = []
    for number in question_numbers:
        questions_texts.append(testbank[number])
    return questions_texts

def parse_questions_xml(file_name):
    tree = ET.ElementTree(file=file_name)
    root = tree.getroot()
    questions_list = []
    # for crappy formatting
    questions_list = []
    for item in root:
        #print(item.attrib['title'])
        text = item.attrib['title']
        for question in item:
            answers = []
            for data in question:
                try:
                    validity = data.attrib['valid']
                    if validity == 'valid':
                        answer = '+'
                except KeyError:
                    answer = '-'
                if data.text == '\n        ':        
                    for secsublevel in data:
                        #print(answer, secsublevel.text)
                        answers.append({'option': secsublevel.text, 'answer': answer})
                else:
                    answers.append({'option': data.text, 'answer': answer})
            #print()
            question = {'q': text, 'a': answers}
            if len(question['a']) > 3:
                questions_list.append(question)
    return questions_list

def parse_questions_properlyformatted_xml(file_name):
    tree = ET.ElementTree(file=file_name)
    root = tree.getroot()
    questions_list = []
    # properly formatted case
    for item in root:
        #print(item.attrib['title'])
        text = item.attrib['title']
        for question in item:
            answers = []
            for data in question:
                try:
                    validity = data.attrib['valid']
                    if validity == 'valid':
                        answer = '+'
                except KeyError:
                    answer = '-'
                for secsublevel in data:
                    #print(answer, secsublevel.text)
                    answers.append({'option': secsublevel.text, 'answer': answer})
            question = {'q': text, 'a': answers}
            questions_list.append(question)
    return questions_list

def parse_questions_txt(infile):
    data = open(infile, 'r')#, encoding = "ISO-8859-2")
    lines_list = []
    for line in data.readlines():
        newline = line.strip().split('\n')
        lines_list.append(newline)
    lastline = len(lines_list)
    questions_list = []
    for line in range(len(lines_list)):
        if lines_list[line][0][1:7] == "[multi": # find headline of question    
            text = lines_list[line+1][0]
            answers = []
            if line + 7 > lastline:
                last_to_check = lastline - line
            else:
                last_to_check = 7
            for option_line in range(2,last_to_check):
                answer = lines_list[line+option_line][0][:1]
                option = lines_list[line+option_line][0][2:]
                if answer == '+' or answer =='-':
                    answers.append({'option': option, 'answer': answer})
            question = {'q': text, 'a': answers}
            questions_list.append(question)
    return questions_list

def parse_studentdata(workbook):
    wb = load_workbook(workbook)
    ws = wb.active
    students_list = []
    group = ws['B2'].value
    line = 1
    while True:
        if ws['A'+str(line)].value == "Students" :
            firstline = line+2
            break
        if line > 100:
            print('No students found.')
            break
        line = line + 1
    line = firstline
    while True:
        name_index = 'B'+str(line)
        code_index = 'A'+str(line)
        name = ws[name_index].value
        if name!= None:
            st_code = ws[code_index].value            
            new_student = {'name': name, 'code': st_code, 'group': group}
            students_list.append(new_student)
            line = line + 1
        else:
            break
    return students_list