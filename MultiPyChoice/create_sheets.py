# These functions enable the creation of the empty test sheets

import os
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import qrcode
from PyPDF2 import PdfFileMerger
import openpyxl
from give_empty_image import *


def totally_unwanted_segmentation_trick(test_id):
    try:
        name_split = test_id.split('  ')
        group_split = test_id.split('.')
        if group_split[1][0] == 'p':
            name = ''
        else:
            name = name_split[0]
        for s in group_split:
            if len(s) > 1:
                if (s[len(s)-1]).isdigit() == True:
                    group = s[len(s)-2:]
                    break
                
        dlength = len(group_split)
        year = group_split[dlength-4]
        month = group_split[dlength-3]
        day = group_split[dlength-2]
        date = year + '.' + month + '.' + day
        return name, group, date
    except:
        return None, None, None

def generate_qr(data):
    qr = qrcode.QRCode(version=1, box_size=10, border=0)
    qr.add_data(data)
    qr.make()
    img = qr.make_image(fill_color="black", back_color="white")
    return img

def create_page(empty_img, test_id, hashed_id, test_lang, test_len):
    #print(test_id)
    img_qr = generate_qr(hashed_id)
    if empty_img == 'unused':
        img1 = give_empty_image(test_lang)
    else:
        img1 = Image.open(empty_img)
    
    draw = ImageDraw.Draw(img1)
    # xy = [(x0, y0), (x1, y1)]
    # cover unneeded answer numbers and their corresponding circles
    lower_limit = (1352, 2030)
    x0 = 800
    length_pairs = [(30, 338),(35,643),(40,920),(45,1200),(50,1468),(55,1734),(60,2030)]
    for pair in length_pairs:
        if pair[0] == test_len:
            y0 = pair[1]
            break
        else:
            if test_len > 60: y0 = 2030
            else: y0 = 338
    upper_limit = (x0,y0)
    draw.rectangle([upper_limit, lower_limit], fill=(255,255,255), outline=None, width=1)
    img_qr = img_qr.resize((300,300))
    img1.paste(img_qr, (1140,5))
    draw = ImageDraw.Draw(img1)
    font = ImageFont.truetype('./fonts/LiberationSans-Regular.ttf', 36)
    name = test_id[0]
    group = test_id[1]
    topic = test_id[2]
    date = test_id[3]
    test_id = name
        
    firstline = name + ', '
    if test_lang == 'hu':
        firstline = firstline + str(group) #+ '. csoport'
    
    if test_lang == 'en':
        firstline = firstline + str(group)#'Group ' + str(group)
    
    secondline = date+', '+topic
    
    #draw.text((70, 40),test_id,(0,0,0), font = font)
    draw.text((70, 40), firstline, (0,0,0), font = font)
    draw.text((70, 140), secondline, (0,0,0), font = font)
    #draw.text((70, 190),"Dátum: " + date,(0,0,0), font = font)
    pdf_out = img1.convert('RGB')
    out_pdf_name = name+'_'+group+'.pdf'
    pdf_out.save(out_pdf_name)
    return out_pdf_name