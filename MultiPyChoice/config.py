
def get_config():
    return {
        'localization': 'en',
        'grades': {2: 60, 3: 70, 4: 80, 5: 90},
        'image_processing': {'png_ppi': 100,
                            'enhance_png_contrast': 1.12}
        }

def question_compositions():
    pass


def localize_en():
    return {
        'browsebutton': 'Browse',
        'errors': {
            '60_line1': 'The number of questions exceeds 60:',
            '60_line2': 'Please, reduce the number of questions.',
            'cv': 'cannot be evaluated (computer vision error).',
            'missing': 'cannot be evaluated (student unidentified – wrong salt or page missing from solution database).',
            'no_questiondb': 'Please, select at least one valid question database.',
            'qr': 'cannot be evaluated (QR error).',
            'questions_notparsed': 'could not be parsed.',
            'solution_notparsed': 'Solution database could not be parsed.',
            'students_notparsed': 'Student data was not parsed.',
            'qdb_toomany': 'The number of selected questions exceeds the number of questions in:'},
        'evaluation': {
            '2': '2 (Pass)',
            '3': '3 (Satisfactory)',
            '4': '4 (Good)',
            '5': '5 (Excellent)',
            'evalbutton': 'Evaluate',
            'exportbutton': 'Export',
            'exportfile': 'Filename to export:',
            'files': 'Files',
            'grading': 'Grading criteria %',
            'scanfolder': 'Folder containing the scanned pages',
            'solutions': 'Solution database:', 'title': 'Evaluation'
            },
        'plots': {
            'message': 'Work in progress.\nIn the future, student statistics shall be here.',
            'title': 'Plots'
            },
        'questions': {
            'addbutton': 'Add',
            'db': 'Database ',
            'title': 'Questions'
            },
        'salt': 'Password (salt):',
        'success': {
            'answersheets_done': 'Answer sheets were created:',
            'answersheets_gen': 'Generating answer sheets...',
            'evaluated': 'students were evaluated.',
            'export': 'Successful export:',
            'processing_images': 'Processing images... (might take a couple of minutes)',
            'student': 'Student:',
            'score': 'Score:',
            'grade': 'Grade:'
            },
        'testcreation': {
            'createbutton': 'Create',
            'en': 'English',
            'files': 'Files',
            'groupquestions': 'Same questions within groups',
            'hu': 'Hungarian',
            'language': 'Language',
            'no': 'No',
            'newsolution': 'Name for new solution database file:',
            'newsolution_filename': 'Solution',
            'topic': 'Topic of the test:',
            'prevsolution': 'Previous solution database (just sheets):',
            'sheetsbutton': 'Answer sheets',
            'students': 'Students database:',
            'title': 'Test creation',
            'yes': 'Yes',
            'encoding': 'Text encoding'
            },
        'page': {
            'name': 'Name: ',
            'group': 'Group: '
            }
        }

def localize_hu():
    return {
        'browsebutton': 'Választás',
        'errors': {
           '60_line1': 'Több, mint 60 kérdés került kiválasztásra:',
           '60_line2': 'Csökkentse a kiválasztott kérdések számát.',
           'cv': 'nem értékelhető (computer vision hiba).',
           'missing': 'nem értékelhető (nem azonosított hallgató: salt hiba vagy nincs a megoldókulcsban).',
           'no_questiondb': 'Válasszon legalább egy megfelelő kérdésadatbázist.',
           'qr': 'nem értékelhető (QR hiba).',
           'questions_notparsed': 'fájl nem értelmezhető.',
           'solution_notparsed': 'A megoldókulcs adatbázis nem értelmezhető.',
           'students_notparsed': 'A hallagtói adatbázis nem értelmezhető.',
           'qdb_toomany': 'Több kérdést próbál kiolvasni a fájlból, mint ahány kérdést tartalmaz:'},
        'evaluation': {
            '2': '2 (Elégséges)',
            '3': '3 (Közepes)',
            '4': '4 (Jó)',
            '5': '5 (Jeles)',
            'evalbutton': 'Értékelés',
            'exportbutton': 'Exportálás',
            'exportfile': 'Exportált fájl neve:',
            'files': 'Fájlok',
            'grading': 'Osztályzatok határai (%)',
            'scanfolder': 'A scannelt oldalakat tartalmazó mappa:',
            'solutions': 'Megoldásokat tartalmazó adatbázis:',
            'title': 'Értékelés'
            },
        'plots': {
            'message':
            'Folyamatban.\nA jövőben a hallgatók statisztikái lesznek itt láthatók.',
            'title': 'Ábrák'
            },
        'questions': {
            'addbutton': 'Hozzáadás',
            'db': 'Adatbázis ',
            'title': 'Kérdések'
            },
        'salt': 'Jelszó (salt):',
        'testcreation': {
            'newsolution': 'Az új megoldásadatbázis neve:',
            'newsolution_filename': 'Megoldások',
            'createbutton': 'Indítás',
            'en': 'Angol',
            'files': 'Fájlok',
            'groupquestions': 'Csoportokon belül azonos kérdések',
            'hu': 'Magyar',
            'language': 'Nyelv',
            'no': 'Nem',
            'prevsolution': 'Korábbi megoldókulcs (csak megoldólapok készítése):',
            'sheetsbutton': 'Csak megoldólap',
            'students': 'Hallgatókat tartalmazó adatbázis:',
            'title': 'Tesztkészítés',
            'yes': 'Igen',
            'encoding': 'Szöveg kódolása',
            'topic': 'Témakör'
            },
        'success': {
            'answersheets_done': 'Megoldólapok elkészültek:',
            'answersheets_gen': 'Megoldólapok készítése...',
            'evaluated': 'hallgató kapott osztályzatot.',
            'export': 'Sikeres exportálás:',
            'processing_images': 'Scannelt oldalak feldolgozása... (igénybe vehet néhány percet)',
            'student': 'Hallgató:',
            'score': 'Pontszám:',
            'grade': 'Érdemjegy:'
            },
        'page': {
            'group': 'Csoport: ',
            'name': 'Név: '
            }
        }