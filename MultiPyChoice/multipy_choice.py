# import dependencies
import numpy as np
import os
import csv
import PySimpleGUI as sg
import openpyxl
from hashlib import md5 as hashlib_md5
from fpdf import FPDF
from datetime import date
from pdf2image import convert_from_path

# import parts of the script
from config import *
from create_sheets import *
from image_identification import *
from choice_recognition import *
from show_images import *
from create_tests import *

__version__ = '0.0.1'


def read_pages(location, settings, poppler_path):
    contrast = settings['enhance_png_contrast']
    page_counter = 0
    ingested_pages = []
    pdf_list = []
    image_list = []
    for file in os.listdir():
        if (str(file)[-4:]).lower() == ".pdf":
            pdf_list.append(file)
        if (str(file)[-4:]).lower() == ".jpg":
            image_list.append(file)
            
    for file in pdf_list:        
        if sys.platform == "win32":
            pages = convert_from_path(file, settings['png_ppi'], poppler_path = poppler_path)
        else:
            pages = convert_from_path(file, settings['png_ppi'])
        for page in pages:
            page.save('out.png', 'PNG')
            lowcon = Image.open('out.png')
            if contrast == 0:
                lowcon.save('scanned_image_'+str(page_counter)+'.png')
            else: 
                enh_con = ImageEnhance.Contrast(lowcon)
                image_contrasted = enh_con.enhance(contrast) # might help with grading but breaks some QR codes
                image_contrasted.save('scanned_image_'+str(page_counter)+'.png')
            ingested_pages.append('scanned_image_'+str(page_counter)+'.png')
            page_counter = page_counter + 1
    
    for file in image_list:
        lowcon = Image.open(file)
        #lowcon = lowcon.transpose(method=Image.ROTATE_180)
        if contrast == 0:
            lowcon.save('scanned_image_'+str(page_counter)+'.png')
        else: 
            enh_con = ImageEnhance.Contrast(lowcon)
            image_contrasted = enh_con.enhance(contrast) # might help with grading but breaks some QR codes
            image_contrasted.save('scanned_image_'+str(page_counter)+'.png')
        ingested_pages.append('scanned_image_'+str(page_counter)+'.png')
        page_counter = page_counter + 1
    
    return ingested_pages

def ingest_solution(solution_database, salt):
    try:
        if os.path.isfile(solution_database):
            solution_list = list(csv.reader(open(solution_database, 'r', encoding = "utf-8"), delimiter=','))
            students = []
            for line in range(len(solution_list)):
                if solution_list[line][0] == '[BEGIN]':
                    full_id = solution_list[line+1]
                    try:
                        name, group, legacy_topic, date = full_id[0], full_id[1], full_id[2], full_id[3]
                    except IndexError:
                        print('Index error, full_id is used as name')
                        name = full_id
                        group = ''
                        date = ''
                    hashed_id = hashlib_md5(bytes(salt+str(full_id), 'utf-8')).hexdigest()
                    start_solution = line+2
                if solution_list[line][0] == '[END]':
                    test_length = int(solution_list[line-1][0])
                    students.append({'test_id': full_id, 'hashed_id': hashed_id, 'name': name, 'group': group, 'date': date ,'begin_line': start_solution, 'test_len': test_length, 'legacy_topic': legacy_topic})
            return solution_list, students
        else:
            return [], []
    except ValueError:
        return [], []

def check_grading(grade2,grade3,grade4,grade5):
    if grade2 < grade3:
        if grade3 < grade4:
            if grade4 < grade5:
                return True
            else: return False
            
def give_grade(score,maxscore,grade2,grade3,grade4,grade5):
    percent = 100 * score / maxscore
    if percent >= grade5:
        return 5
    if percent >= grade4:
        return 4
    if percent >= grade3:
        return 3
    if percent >= grade2:
        return 2
    else:
        return 1

def export_grades(students, outfile):
    wb = openpyxl.Workbook()
    sheet = wb.active
    sheet.title = 'Grades'
    wb.create_sheet(index = 1, title = 'Errors')
    wb['Grades']['A1'] = 'Name'
    wb['Grades']['B1'] = 'Group'
    wb['Grades']['C1'] = 'Grade'
    wb['Grades']['D1'] = 'Score'
    namecol = 'A'
    groupcol = 'B'
    gradecol ='C'
    scorecol = 'D'
    gline = 2
    eline = 1
    for i in range(len(students)):
        errors = True
        try:
            grade = students[i]['grade']
            if students[i]['errors'] == []:
                errors = False
        except KeyError:
            grade = False
        if grade != False:
            wb['Grades'][namecol+str(gline)] = students[i]['name']
            wb['Grades'][groupcol+str(gline)] = students[i]['group']
            wb['Grades'][gradecol+str(gline)] = students[i]['grade']
            wb['Grades'][scorecol+str(gline)] = students[i]['score']
            gline = gline+1
            if errors != False:
                wb['Errors']['A'+str(eline)] = students[i]['name']
                eline = eline + 1
                for k in range(len(students[i]['errors'])):
                    qnumber = students[i]['errors'][k][0][0]
                    correct = ''
                    given = ''
                    for c in range(1,5):
                        correct = correct + students[i]['errors'][k][0][c] + ' '
                    for g in range(0,4):
                        given = given + students[i]['errors'][k][1][g] + ' '
                    wb['Errors']['A'+str(eline)] = qnumber
                    wb['Errors']['B'+str(eline)] = correct
                    wb['Errors']['C'+str(eline)] = given
                    eline = eline + 1                
    outfile_name = outfile+'.xlsx'
    wb.save(outfile_name)
    return outfile_name

def create_answer_sheets(solution_file, test_lang, salt, questions_pdf_list=None):
    if questions_pdf_list == None:
        skip_questions = True
    else:
        skip_questions = False
        
    solution_list, students = ingest_solution(solution_file, salt)
    if solution_list == [] or students == []:
        return None
    else:            
        empty_page = 'unused' # insert an actual png here, if you want to use your own
        merge_list = []
        for student in range(len(students)):
            new_pdf = create_page(empty_page, students[student]['test_id'], students[student]['hashed_id'], test_lang, students[student]['test_len'])
            if skip_questions == False:
                merge_list.append(questions_pdf_list[student])
            merge_list.append(new_pdf)
        
        print(merge_list)
        merger = PdfFileMerger()
        for page in merge_list:
            merger.append(page)
        merged_name = (str(solution_file)[:-4])+'-TESTSHEETS.pdf'
        merger.write(merged_name)
        merger.close()
        for file in merge_list:
            try:
                os.remove(file)
            except:
                pass
        return merged_name

def main():
    # Intiation
    poppler_path = os.getcwd()+'./poppler-21.10.0/Library/bin'
    config = get_config()
    if config['localization'] == 'hu':
        local_text = localize_hu() # more languages can be added with elif statement
    else:
        local_text = localize_en()
        
    sg.ChangeLookAndFeel('Reddit')
    qlines = 0    
    
    # Evaluation tab layout
    export_filename = 'Scores'
    columnev1 = [
        [sg.Frame(local_text['evaluation']['files'],[
        [sg.Text(local_text['evaluation']['solutions'], auto_size_text=False,justification='left')],
        [sg.InputText(os.getcwd(), key='-EVAL-CREATE-SOLUTIONDB-',size=(60,1)),sg.FileBrowse(local_text['browsebutton'])],
        [sg.Text(local_text['evaluation']['scanfolder'], auto_size_text=False, justification='left')],
        [sg.InputText(os.getcwd(), key='-WDIR-', size=(60,1)), sg.FolderBrowse(local_text['browsebutton'])],
        [sg.Text(local_text['evaluation']['exportfile'])],
        [sg.InputText(export_filename, key='-OUTSCORE-FILE-',size=(60,1)), sg.Text('.xlsx')] 
        ])]
        ]
    percent_column = [
        [sg.InputText(config['grades'][2], key = 'GRADE-2', size=(4,1))],
        [sg.InputText(config['grades'][3], key = 'GRADE-3', size=(4,1))],
        [sg.InputText(config['grades'][4], key = 'GRADE-4', size=(4,1))],
        [sg.InputText(config['grades'][5], key = 'GRADE-5', size=(4,1))]
        ]
    
    marks_column = [
        [sg.Text(local_text['evaluation']['2'], size=(12,1))],
        [sg.Text(local_text['evaluation']['3'], size=(12,1))],
        [sg.Text(local_text['evaluation']['4'], size=(12,1))],
        [sg.Text(local_text['evaluation']['5'], size=(12,1))]
        ]
    
    columnev2 = [
        [sg.Frame(local_text['evaluation']['grading'],[
            [sg.Column(marks_column), sg.Column(percent_column)]])]
        
        ]
         
    evaluation_layout = [[sg.Column(columnev1),sg.Column(columnev2)],
                         [sg.Text()],
                         [sg.Button(local_text['evaluation']['evalbutton'], key ='-START-', size=(12,1),button_color=('white', 'green'), disabled = False),
                          sg.Button(local_text['evaluation']['exportbutton'], key = '-EXPORT-', size=(12,1),button_color=('white', 'blue'), disabled = True)]]
    
    # Creation tab layout  
    columncr1 = [
        [sg.Frame(local_text['testcreation']['files'],[
        [sg.Text(local_text['testcreation']['students'], auto_size_text=False,justification='left')],
        [sg.InputText(os.getcwd(), key='-STUDENTS-WS-',size=(60,1)),sg.FileBrowse(local_text['browsebutton'])],
        [sg.Text(local_text['testcreation']['prevsolution'], auto_size_text=False,justification='left')],
        [sg.InputText(os.getcwd(), key='-CREATE-SOLUTIONDB-',size=(60,1)),sg.FileBrowse(local_text['browsebutton'])],
        [sg.Text(local_text['testcreation']['newsolution'], auto_size_text=False,justification='left')],
        [sg.InputText(local_text['testcreation']['newsolution_filename'], key='-OUTSOLUTIONS-FILE-',size=(60,1)), sg.Text('.txt')],
        [sg.Text(local_text['testcreation']['topic'], auto_size_text=False,justification='left')],
        [sg.InputText(' ', size=(20,1), key = '-TEST-TOPIC-')]
        ])],
        [sg.Frame(local_text['testcreation']['language'],[
                  [sg.Radio(local_text['testcreation']['hu'],"RADIO1", default = True, key = 'LANG-HU')],
                  [sg.Radio(local_text['testcreation']['en'],"RADIO1", key = 'LANG-EN')]
                  ]),
         sg.Frame(local_text['testcreation']['groupquestions'],[
                  [sg.Radio(local_text['testcreation']['yes'],"RADIO2", key = '-GROUP-Q-')],
                  [sg.Radio(local_text['testcreation']['no'], "RADIO2", default = True)]
                  ]),
         sg.Frame(local_text['testcreation']['encoding'],[
                  [sg.Radio('UTF-8',"RADIO3", default = True)],
                  [sg.Radio('WINDOWS-1252',"RADIO3", key = '-WINDOWS-1252-')]
                  ])
         ]]
    
    columncr2 = []
    
    creation_layout = [[sg.Column(columncr1), sg.Column(columncr2)],
                       [sg.Text('')],
                       [sg.Button(local_text['testcreation']['createbutton'], key = '-CREATE-', size=(12,1),button_color=('white', 'green'), disabled = False),
                       sg.Button(local_text['testcreation']['sheetsbutton'], key = '-JUST-SHEETS-', size=(12,1),button_color=('white', 'BLUE'))]]
    
    questions_layout = [
        [sg.Frame('',[
        [sg.Frame(local_text['questions']['db']+str(qlines+1),[
        [sg.Slider(range = (1,60), key = '-QNO0-', default_value = 5,resolution = 1, orientation = 'h', size=(20,8)),
         sg.InputText(os.getcwd(), key='-QDB0-',size=(50,8)),sg.FileBrowse(local_text['browsebutton'])],
        ])]], key = 'QFRAME')],
        [sg.Button(local_text['questions']['addbutton'], key = '-ADD-QLINE-', size=(12,1),button_color=('white', 'blue'))]]
    
    plots_layout = [[sg.Text(local_text['plots']['message'])]]
    
    window_layout = [
        [sg.Text(local_text['salt'], size=(18, 1), auto_size_text=False,justification='left'), sg.InputText('', size=(20,1), key = '-SALT-')],
        [sg.Text()],
        [sg.TabGroup([[sg.Tab(local_text['evaluation']['title'], evaluation_layout),
                       sg.Tab(local_text['testcreation']['title'], creation_layout),
                       sg.Tab(local_text['questions']['title'], questions_layout),
                       sg.Tab(local_text['plots']['title'], plots_layout)]])],
        #[sg.Output(size=(110,10))]
        ]
    
    window = sg.Window('MultiPyChoice v'+__version__, window_layout)
    
    while True:
        event, values = window.read(timeout=100)
        if event in (sg.WIN_CLOSED, 'Exit'):
            break
    
        elif event == '-START-':
            if check_grading(int(values['GRADE-2']),int(values['GRADE-3']), int(values['GRADE-4']),int(values['GRADE-5'])) == True:
                print(local_text['success']['processing_images'])
                print()
                grades_given = 0
                os.chdir(values['-WDIR-'])
                location = ''
                pages = read_pages(location, config['image_processing'], poppler_path)
                solution_file = values['-EVAL-CREATE-SOLUTIONDB-']
                solution_list, students = ingest_solution(solution_file, str(values['-SALT-']))
                solution_error = False
                if solution_list == [] or students == []:
                    print(local_text['errors']['solution_notparsed'])
                    solution_error = True
                
                if solution_error == False:
                    grading_success = False
                    for page in range(len(pages)):
                        # Identifying the test and the orientation of the scanned page
                        paper_id, rotated = identify_test(pages[page])
                        if paper_id == '': # final try, manual input from a crop of the headline of the page
                            paper_id, rotated = identify_test_manual(pages[page], students)
                        
                        # TODO: make up list from id'd and not id'd pages, and retry non id'd ones in the end
                        
                        # Setting orientation
                        if paper_id != '':
                            if rotated:
                                img = cv2.imread(pages[page])
                                img = cv2.rotate(img, cv2.cv2.ROTATE_180)
                            else:
                                img = cv2.imread(pages[page])
                                
                            # Reading answers
                            decode_success, pixel_array = handle_image(img)
                            if decode_success == True:
                                answers = find_choices(pixel_array)
                                counter = 1
                                answers_readable = []
                                for line in range(len(answers)):
                                    if line % 6 != 0:
                                        answers_readable.append(answers[line-1])
                                        counter = counter + 1
                                id_matched = False
                                
                                for student in range(len(students)):
                                    if paper_id == students[student]['hashed_id']:
                                        current_student = students[student]
                                        print(local_text['success']['student'], current_student['name']+', Group',current_student['group'])
                                        line_offset = current_student['begin_line']
                                        id_matched = True
                                        break
                                
                                if id_matched == True:
                                    score = 0.0
                                    error_lines = []
                                    maxscore = int(current_student['test_len'])*4
                                    for line in range(current_student['test_len']):
                                        error_in_line = False
                                        for circle in range(0,4):
                                            if solution_list[line+line_offset][circle+1] == answers_readable[line][circle]:
                                                score = score + 1
                                            else:
                                                score = score - 0.5
                                                error_in_line = True
                                        if error_in_line == True:
                                            #print(solution_list[line+line_offset], answers_readable[line])
                                            error_lines.append([solution_list[line+line_offset], answers_readable[line]])

                                    students[student]['score'] = score
                                    grade = give_grade(score, maxscore, int(values['GRADE-2']),int(values['GRADE-3']),
                                                       int(values['GRADE-4']),int(values['GRADE-5']))
                                    students[student]['grade'] = grade
                                    students[student]['errors'] = error_lines
                                    print(local_text['success']['score'], score, local_text['success']['grade'], grade)
                                    print()
                                    grades_given = grades_given + 1
                                    grading_success = True
                                else:
                                    print(pages[page], local_text['errors']['missing'])
                                    print()
                            else:
                                print(pages[page], local_text['errors']['cv'])
                                print()
                        else:
                            print(pages[page], local_text['errors']['qr'])
                            print()
                    if grading_success == True:
                        window['-EXPORT-'].update(disabled = False)
            print(str(grades_given)+'/'+str(len(students)), local_text['success']['evaluated'])                
            
        elif event == '-EXPORT-':
            if students != []:
                outfile = values['-OUTSCORE-FILE-']
                outfile_name = export_grades(students, outfile)
                print(local_text['success']['export'])
                print(values['-WDIR-']+'/'+outfile_name)
                print()

        elif event == '-CREATE-':
            # Creating solution file, and then creating answer sheets for it.
            questions_list = []
            questions_ready = False
            students_ready = False
            same_for_group = values['-GROUP-Q-']
            topic = values['-TEST-TOPIC-']
            letters = ('    A) ', '    B) ', '    C) ', '    D) ', '    E) ')
            questions_pdf_list = []
            if values['LANG-HU'] == True:
                test_lang = 'hu'
            else:
                test_lang = 'en'
            
            # Parsing questions from disk
            for db in range(0,qlines+1):
                question_database = values[f'-QDB{db}-']
                question_number = int(values[f'-QNO{db}-'])
                db_filename = os.path.basename(values[f'-QDB{db}-'])
                question_category, question_extension = os.path.splitext(db_filename)
                if os.path.isfile(question_database):
                    try: # to debug create_test.py, try running it with the problematic file
                        questions = parse_questions_xml(question_database)
                        questions_list.append({'questions': questions, 'number': question_number, 'category': question_category})
                    except:
                        try:
                            questions = parse_questions_txt(question_database)
                            questions_list.append({'questions': questions, 'number': question_number, 'category': question_category})
                        except UnicodeDecodeError:
                            print(question_database, local_text['errors']['questions_notparsed'])
            
            # Checking if the number of questions exceeds 60 (physical limit of the answer sheet)
            questions_ready = True
            if questions_list != []:
                test_length = 0
                for question_db in questions_list:
                    test_length = test_length + question_db['number']
                    print(question_db)
                if test_length < 61:
                    pass
                else:
                    print(local_text['errors']['60_line1'], test_length)
                    print(local_text['errors']['60_line2'])
                    questions_ready = False
            else:
                print(local_text['errors']['no_questiondb'])
                
            for category in questions_list:
                if len(category['questions']) < category['number']:
                    print(local_text['errors']['qdb_toomany'], category['category'])
                    questions_ready = False
            
            if questions_ready == True:
                try:
                    students = parse_studentdata(values['-STUDENTS-WS-'])
                    students_ready = True
                except:
                    print(local_text['errors']['students_notparsed'])
            
            # Generating individual question lists for each student or group
            if questions_ready == True and students_ready == True:
                if same_for_group == True:
                    groups = []
                    for student in students:
                        if student['group'] in groups:
                            pass
                        else:
                            groups.append(student['group'])
                    group_questions_list = []
                    for group in groups:
                        selected_questions = []
                        for category in questions_list:
                            question_numbers = random.sample(range(0, len(category['questions'])), category['number'])
                            category_question_texts = get_questions_texts(category['questions'], question_numbers)
                            for question in category_question_texts:
                                selected_questions.append(question)
                        group_questions_list.append({'group': group, 'questions': selected_questions})
                        
                else:
                    student_questions_list = []
                    for student in students:
                        selected_questions = []
                        for category in questions_list:
                            question_numbers = random.sample(range(0, len(category['questions'])), category['number'])
                            category_question_texts = get_questions_texts(category['questions'], question_numbers)
                            for question in category_question_texts:
                                selected_questions.append(question)
                        student_questions_list.append({'student': student['name'], 'questions': selected_questions})
                
                # Generating test and solution
                solution_output = []
                for student in range(len(students)):
                    lines_to_print = []
                    lines_to_solution = []
                    lines_to_solution.append('[BEGIN]')
                    lines_to_solution.append(students[student]['name']+','+students[student]['code']+','+students[student]['group']+','+topic+','+str(date.today()))
                    if same_for_group == True:
                        for group in range(len(group_questions_list)):
                            if group_questions_list[group]['group'] == students[student]['group']:
                                questions_texts = group_questions_list[group]['questions']
                    else:
                        questions_texts = student_questions_list[student]['questions']
                    random.shuffle(questions_texts)
                    for j in range(len(questions_texts)):
                        newline = str(j+1)+'/ '+questions_texts[j]['q']
                        solution_line = str(j+1)+','
                        options = questions_texts[j]['a']
                        random.shuffle(options)
                        lines_to_print.append(newline)
                        letter_count = 0
                        for i in range(len(options)):
                            newline = letters[letter_count]+str(options[i]['option'])#+' '+str(options[i]['answer'])
                            solution_line = solution_line + str(options[i]['answer'])
                            if i < len(options)-1:
                                solution_line = solution_line + ','
                            lines_to_print.append(newline)
                            letter_count = letter_count + 1
                        lines_to_print.append('')
                        lines_to_solution.append(solution_line)
                    lines_to_solution.append('[END]')
                    # Adding this solution to the rest
                    for line in lines_to_solution:
                        #print(line)
                        solution_output.append(line)
                    
                    # Creating PDF with questions
                    firstline = local_text['page']['name']+str(students[student]['name'])+' '+local_text['page']['group']+str(students[student]['group']+' '+str(topic)+' '+str(date.today()))                  
                    pdf = FPDF(format='A4')
                    #pdf = fpdf.FPDF(format='A4')
                    pdf.add_page()
                    pdf.add_font('LiberationSans', 'B', './fonts/LiberationSans-Bold.ttf', uni=True)
                    pdf.add_font('LiberationSans', '', './fonts/LiberationSans-Regular.ttf', uni=True)
                    pdf.set_font('LiberationSans', 'B', size=10)
                    pdf.write(4,str(firstline))
                    pdf.ln()
                    pdf.set_font("LiberationSans",size=10)
                    pdf.write(4,'-----------------------------------------------------------------------------')
                    pdf.ln()
                    linecount = 5
                    for line in range(len(lines_to_print)):
                        if lines_to_print[line] == '':
                            if linecount > 60:
                                for i in range(0,7):
                                    pdf.ln()
                                linecount = 1
                        print(lines_to_print[line])
                        pdf.write(4,str(lines_to_print[line]))
                        pdf.ln()
                        linecount = linecount + 1
                    outfile_pdf = 'Test_'+str(student)+'.pdf'
                    pdf.output(outfile_pdf, 'F')
                    questions_pdf_list.append(outfile_pdf)
            
            # Generating complete solution file
            solution_outfile = str(values['-OUTSOLUTIONS-FILE-'])+'.txt'
            outfile = open(solution_outfile, 'w', encoding = 'UTF-8')
            for line in solution_output:
                outfile.write(line+'\n')
            outfile.close()
            print()
            window['-CREATE-SOLUTIONDB-'].update(solution_outfile)           
            answer_sheet = create_answer_sheets(solution_outfile, test_lang, values['-SALT-'], questions_pdf_list)
            
        elif event == '-JUST-SHEETS-':
            # Creating answer sheets based on an already existing solution file.
            print(local_text['success']['answersheets_gen'])
            if values['LANG-HU'] == True:
                test_lang = 'hu'
            else:
                test_lang = 'en'
                
            solution_file = values['-CREATE-SOLUTIONDB-']
            
            final_filename = create_answer_sheets(solution_file, test_lang, values['-SALT-'])
            
            if final_filename == None:
                print(local_text['errors']['solution_notparsed'])
            else:
                print(local_text['success']['answersheets_done'])
                print(final_filename)
            
        
        elif event == '-ADD-QLINE-':
            qlines = qlines + 1
            window.extend_layout(window['QFRAME'],[
                [sg.Frame(local_text['questions']['db']+str(qlines+1),[
                [sg.Slider(range = (1,60), key = f'-QNO{qlines}-', default_value = 20,resolution = 1, orientation = 'h', size=(20,8)),
                 sg.InputText(os.getcwd()+'', key= f'-QDB{qlines}-',size=(50,8)),sg.FileBrowse(local_text['browsebutton'])]])]
                ])

if __name__ == '__main__':
    main()